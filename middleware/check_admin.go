package middleware

import (
	"backend-github-trending/model"
	req2 "backend-github-trending/model/req"
	"github.com/labstack/echo/v4"
	"net/http"
)

// Một MiddlewareFunc là một hàm nhận vào một HandlerFunc và trả về một HandlerFunc khác.
func ISAdmin() echo.MiddlewareFunc {

	// HandlerFunc là function nhận param là echo.Context và return ra error
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			req := req2.ReqSignIn{}

			if err := c.Bind(&req); err != nil {
				return c.JSON(http.StatusBadRequest, model.Response{
					StatusCode: http.StatusBadRequest,
					Message:    err.Error()})
			}

			if req.Email != "admin@gmail.com" {
				return c.JSON(http.StatusBadRequest, model.Response{
					StatusCode: http.StatusBadRequest,
					Message:    "Bạn không có quyền thực hiện chức năng này!",
					Data:       nil,
				},
				)
			}

			return next(c)
		}
	}
}
